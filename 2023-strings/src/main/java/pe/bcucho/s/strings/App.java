    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.strings;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class App {
    public static void main(String[] args) {
        String string=" Bryan Cucho "; //String es una clase
        System.out.println("string = " + string);
        
        //Length
        System.out.println("string size " + string.length()); //Toma el tamño del espacio
        //Character Location
        
        System.out.println("first character of the string " + string.charAt(0));
        System.out.println("first character of the string " + string.charAt(string.length()-1));
        System.out.println( string.toUpperCase());
        
        
        //split cortar cierto cadena
        String string2 ="Hola, soy el alumno del nombre";
        
        //Creamos un array de string
        // String[] partes = string2.split(" ");
        String[] partes = string2.split(",", 4);
        for(int i=0;i<partes.length ;i++){
            System.out.println(partes[i]);
        }
        //Borra los espacios que se encuentren en los extremos de la cadena 
        String trim = string.trim();
        System.out.println("trim " + trim);
    }
}   
