/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practices1;

import java.util.Scanner;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class UserApp {
    public static void main(String[] args) {
        Circunferencia circle1= new Circunferencia();
        System.out.println(circle1.toString());
        System.out.println("El area del primer objeto es: " + circle1.getArea());
        System.out.println("La longitud del primer objeto es: " + circle1.getLongitud());
        
        Circunferencia circle2= new Circunferencia(7.5);
        System.out.println(circle2.toString());
        System.out.println("El area del segundo objeto es: " + circle2.getArea());
        System.out.println("La longitud del segundo objeto es: " + circle2.getLongitud());
        Circunferencia circle3= new Circunferencia(4.8);
        System.out.println(circle3.toString());
        System.out.println("El area del tercer objeto es: " + circle3.getArea());
        System.out.println("La longitud del tercer objeto es: " + circle3.getLongitud());
 
        System.out.println("Modificación de parametros por el usuario");
        Scanner entrada = new Scanner(System.in);
        System.out.println("Ingrese la nueva area para la primera circunferencia");
        double n=entrada.nextDouble();
        circle1.setArea(n);
        System.out.println("El nuevo radio de la primera circunferencia es:\n" + circle1.toString());

        circle2.setRadious(7.5+3);
        System.out.println("Ingese la nueva longitud para la tercera circunferencia");
        double p=entrada.nextDouble();
        circle3.setLongitud(p);
        System.out.println("El nuevo radio es "+ circle3.toString());


//        Circunferencia a = new Circunferencia();
//       System.out.println("a" + a.Pi);
    }
}
