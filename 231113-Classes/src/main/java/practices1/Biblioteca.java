/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practices1;

import java.util.Scanner;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Biblioteca {
     public static void main(String[] args) {
      Scanner entrada = new Scanner(System.in);
         System.out.println("Bienvenidos a  la biblioteca de la fiee");
         System.out.println("Ingrese el numero de peliculas de la biblioteca");
         int numP= entrada.nextInt();
         //Crear arreglo para almacenar las peliculas 
         
         Pelicula[] peliculas = new Pelicula[numP];
         entrada.nextLine(); //Limpiar el buffer 
         
         for(int i=0;i<numP;i++){
             System.out.println("Ingrese el nombre:");
             String name = entrada.nextLine();
             System.out.println("Ingrese el nombre del director:");
             String di= entrada.nextLine();
             System.out.println("Ingrese la duracion");
             int numD=entrada.nextInt();
             entrada.nextLine();
             //Crear un objeto pelicula con los parametros proporcionados
             peliculas[i] = new Pelicula(name,di,numD);
         }
         
         for(int i=0;i<numP;i++){
             System.out.println("Pelicula " + i+1 + peliculas[i].toString());
         }
    }
}
