/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practices1;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Circunferencia {
    //Final Field
    private static final double Pi=3.1416;
    //Field
    private double radious;
    
    //Constructors
    Circunferencia(){
        this.radious=1; //Inicializar the field 
    }
    
    Circunferencia(double newradious){
        this.radious=newradious;
    }
    
    public void setRadious(double newradious){
        this.radious=newradious;
    }

    public double getDiametro(){
        return (2*this.radious);
    }
    
    public void setDiametro(double diametro){
        this.radious= diametro/2;
    }
    
    public double getLongitud(){
        return (2*Pi*this.radious);
    }
    
    public void setLongitud(double newLongitud){
        this.radious= newLongitud /(2*Pi); 
    }
    
    public double getArea(){
        return (Pi* Math.pow(this.radious,2));
    }
    
    
    public void setArea(double newArea){
        this.radious= Math.sqrt((newArea/Pi));
    }
    
    
    @Override
    public String toString() {
        return "Circunferencia{" + "radious=" + radious + '}';
    }
   
}
