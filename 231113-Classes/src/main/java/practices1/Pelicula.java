/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practices1;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Pelicula {
    //Fields
    private String pelicula;
    private String director;
    private int duracion;
    
    //Constructors
    public Pelicula(){
        this.pelicula="";
        this.director="";
        this.duracion=0;
    }
    
    public Pelicula(String pelicula, String director, int duracion){
        this.pelicula=pelicula;
        this.director=director;
        this.duracion=duracion;
    }

    public void setPelicula(String pelicula){
        this.pelicula=pelicula;
    }
    
    public void setDirector(String director){
        this.director = director;
    }
    
    public void setDuracion (int duracion){
        this.duracion= duracion;
    }
    
    @Override
    public String toString() {
        return "Pelicula{" + "titulo=" + pelicula + ", director=" + director + ", duracion=" + duracion + '}';
    }
    
}
