/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practices1;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Usuario {
    //Fields 
    private String name;
    private int dni;
    
    //Constructor 
    Usuario(){
        this.name="";
        this.dni=0;
    }
    
    Usuario(String name, int dni){
        this.name=name;
        this.dni=dni;
    }

    public void setName(String name){
        this.name=name;
    }
    
    public void setDni(int dni){
        int a=contardigitos(dni);
        if(a==8){
            this.dni=dni;
        }else{
            System.out.println("DNI invalido");
        }
    }
    @Override
    public String toString() {
        return "Usuario{" + "name=" + name + ", dni=" + dni + '}';
    }
    
    private static int contardigitos(int numero){
        //Convertir el numero a string
        String numerostring= Integer.toString(numero);
        int cantidad=numerostring.length();
        return cantidad;
    }
}
