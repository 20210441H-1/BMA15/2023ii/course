/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class NewClass {

    int channel;
    int volumeLevel;
    boolean on;

    public boolean isOn() {
        return on;
    }

    // límites
    final int minVolumeLevel = 1;
    final int maxVolumeLevel = 7;
    final int minChannel = 1;
    final int maxChannel = 120;

    public NewClass() {
        channel = 1;
        volumeLevel = 3;
        on = false;
    }

    public void turnOn() {
        on = true;
    }

    public void turnOff() {
        on = false;
    }

    public void setChannel(int newChannel) {
        channel = newChannel;
    }

    public void setVolume(int newVolumeLevel) {
        volumeLevel = newVolumeLevel;
    }

    public void channelUp() {
        if (on && channel < maxChannel) {
            channel++;
        } else if (channel == maxChannel) {
            channel = minChannel;
        }
    }

    public void channelDown() {
        if (on && minChannel < channel) {
            channel--;
        } else if (channel == minChannel) {
            channel = maxChannel;
        }
    }

    public void volumeUp() {
        if (on && volumeLevel < maxVolumeLevel) {
            volumeLevel++;
        }
    }

    public void volumeDown() {
        if (on && minVolumeLevel < volumeLevel) {
            volumeLevel--;
        }
    }

    @Override
    public String toString() {
        return "TV{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }

}

