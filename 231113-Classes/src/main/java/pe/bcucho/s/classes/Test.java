/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
//public class ShowErrors {
//    int acertijo;
//    public static void main(String[] args) {
//        ShowErrors t = new ShowErrors();
//        System.out.println(t.acertijo);
//    }
//}

//    public class ShowErrors{
//        public static void main(String[] args) {
//            C c = new C(5.0f);
//            System.out.println(c.value);
//        }
//    }
//    
//    class C{
//        float value = 2;
//        public C(float a){
//            this.value=a;
//    }
//        //Se define un constructor sin argumentos por defecto    
//    }

       public class Test{
           public static void main(String[] args) {
               A a = new A();
               a.print();
           }
       }
  //Un rombo pintado es acotada a una clase composicion  student --- nombre(nombre no existe por si sola)
 //Un diamante sin pintar denota una clase de agregacion student --- direccion()
       class A{
           String s;
           A(String newS){
               s=newS;
           }
           
           A(){
               this("Hola mama");
           }
           public void print(){
               System.out.println(s);
           }
       }
