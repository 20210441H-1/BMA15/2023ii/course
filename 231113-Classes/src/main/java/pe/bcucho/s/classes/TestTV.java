/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestTV {

    public static void main(String[] args) {
        NewClass tv1 = new NewClass(); //instanciar una clase
        tv1.turnOn(); //Encender el tv
       
//        System.out.println(tv1.toString());
        if (tv1.isOn()) {
            System.out.println("Encendido !!!");
            int channel = 120;
            int volumeLevel = 7;
            tv1.setChannel(channel);
            tv1.setVolume(volumeLevel);
            System.out.println(tv1.toString());
            tv1.channelUp();
            System.out.println(tv1.toString());
        } else {
            System.out.println("Apagado !!!");
        }
    }
}

