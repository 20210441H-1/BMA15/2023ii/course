/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Circle {

    public static void main(String[] args) {
        System.out.println("Circle !!!");
    }

    //Static Filds --> constantes o contadores
    static int NumberObjects=0;
    //Fields
    private double radious;

    //Constructors
    Circle() {
        this.radious = 1;//Sirve para referenciarse a si mismo
        this.NumberObjects++;
    }

    Circle(double newRadious) {
        this.radious = newRadious;
        this.NumberObjects++;
    }

    //Ctrl + shit + c 
//    Methods
//   Si se instacian 3 objetos, se ejecutan 3 veces el constructor
//    El setter es void pero necesita parametros, 
//    a diferencia del getter que si tiene un valor de retorno pero no tiene funcion de parametro        //El setter le indica le da parametros al objeto
//    Si era negativo debe ser privado, si es mas es publico
    
    static public int getNumber(){
        return NumberObjects;
    }
    public double getArea() {
        return Math.PI * this.radious * this.radious;
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }

    public void setRadious(double radious) {
        //Ese parametro de entrada lo vamos a copiar en nuestro fields
        this.radious = radious;
        //A diferencia de los constructores que solo se ejecutan cuando
        //        se instancia un objeto
        //Los setter son para cambiar los valores

    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }

}
