/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestCircle {

    public static void main(String[] args) {
       
        System.out.println("Test Circle!");
//        Circle cuadraro=new Circle();
//        cuadraro.radious=10; Ocurre si el campo fuera publico
        
        
        Circle circle1; //Creacion del objeto 
        circle1 = new Circle(); //Instanciacion del objeto
        System.out.println(circle1.toString());
        System.out.println("Perimeter 1: " + circle1.getPerimeter());
        System.out.println("Area 1: " + circle1.getArea());
        
        
        double radious2 = 25;
        Circle circle2;
        circle2 = new Circle(radious2);
        System.out.println(circle2.toString());
        System.out.println("Perimeter 2: " + circle2.getPerimeter());
        System.out.println("Area 2: " + circle2.getArea());
        
        System.out.println("");
        Circle circle3 = new Circle();
        System.out.println(circle3.toString());
        double radious3 = 125;
        circle3.setRadious(radious3);
        System.out.println(circle3.toString());
        System.out.println("Perimeter 3: " + circle3.getPerimeter());
        System.out.println("Area 3: " + circle3.getArea());
        System.out.println(Circle.NumberObjects);
        //Un objeto no ncesita ser referenciado 
        //Cada que se escribe new se crea un objeto, indepdiente sepamos donde este o no;

    }
}

