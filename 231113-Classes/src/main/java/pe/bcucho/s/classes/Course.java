/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Course {
    //Fields 
    private String coursename;
    private String [] students;
    private int numberOfStudents; //Por perteecer a la clase se inicializa en 0
    private final int max=100;
    
    public Course(String coursename){
        this.coursename=coursename;
        students = new String[max]; //definimos el maximo de estudiantes;
    }

    public String getCoursename() {
        return coursename;
    }

    public String[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }
    
    void addStudent(String student){
        students[numberOfStudents]= student; 
        numberOfStudents++;
    }
    
    void dropStudent(String student){
        //
    }
}
