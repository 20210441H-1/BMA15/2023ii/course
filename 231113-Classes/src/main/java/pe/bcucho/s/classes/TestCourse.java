/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.classes;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestCourse {
    public static void main(String[] args) {
        System.out.println("TestCourse");
        String[] cursos = {"Estructura de datos", "Sistemas de base de datos", "Programacion orientada a objetos"};
        String[] alumnos = {"Bryan Cucho", "Luis Sanchez", "Anthony"};
        
        Course course1=new Course(cursos[0]);
        Course course2=new Course(cursos[1]);
        Course course3=new Course(cursos[2]);
        
        course1.addStudent(alumnos[0]);
        course1.addStudent(alumnos[2]);
        
        course2.addStudent(alumnos[1]);
        course2.addStudent(alumnos[2]);
        
        course3.addStudent(alumnos[0]);
        course3.addStudent(alumnos[1]);
        
        //Matricula reportes
        System.out.println("Numero de alumnos en el curso " + course1.getCoursename() + ": " + course1.getNumberOfStudents());
        viewStudent(course1);
        System.out.println("Numero de alumnos en el curso " + course2.getCoursename() + ": " + course2.getNumberOfStudents());
        viewStudent(course2);
        System.out.println("Numero de alumnos en el curso " + course3.getCoursename() + ": " + course3.getNumberOfStudents());
        viewStudent(course3);
            
    }
    
    private static void viewStudent(Course course){
        String[] alumnos = course.getStudents();
        for(int i=0;i<course.getNumberOfStudents();i++){
            System.out.print(alumnos[i] + ", ");
        }
        System.out.println("");
    }
 }
