package pe.bcucho.s.nombres;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;

public class App extends Application {

    private Map<String, String> userPasswords = new HashMap<>();
    private final Label lbMessage = new Label("Usuario: ");
    private final TextField tfUsuario = new TextField();
    private final Label lbPassword = new Label("Contraseña (4 dígitos): ");
    private final PasswordField pfContraseña = new PasswordField();
    private final Button bconfirmar = new Button("Iniciar Sesión");
    private final Button bconfirmar2 = new Button("Registrarse");

    @Override
    public void start(Stage stage) {

        HBox hboxUsuario = new HBox(10);
        hboxUsuario.getChildren().addAll(lbMessage, tfUsuario);
        hboxUsuario.setAlignment(Pos.CENTER);

        HBox hboxContraseña = new HBox(10);
        hboxContraseña.getChildren().addAll(lbPassword, pfContraseña);
        hboxContraseña.setAlignment(Pos.CENTER);

        HBox hboxButtons = new HBox(10);
        hboxButtons.getChildren().addAll(bconfirmar, bconfirmar2);
        hboxButtons.setAlignment(Pos.CENTER);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(hboxUsuario);
        borderPane.setCenter(hboxContraseña);
        borderPane.setBottom(hboxButtons);

        var scene = new Scene(new BorderPane(borderPane), 300, 320);
        stage.setScene(scene);
        stage.show();

        bconfirmar.setOnAction(e -> {
            String password = pfContraseña.getText();
            String user = tfUsuario.getText();

            if (user.length() != 4) {
                showAlert("El usuario debe tener 4 caracteres");
            } else if (userPasswords.containsKey(user) && userPasswords.get(user).equals(password)) {
                showAlert("Inicio de sesión exitoso");
            } else {
                showAlert("Inicio de sesión inválido");
            }
            clean();
        });

        bconfirmar2.setOnAction(e -> {
            String user = tfUsuario.getText();
            String password = pfContraseña.getText();

            if (password.length() != 4) {
                showAlert("La contraseña debe tener 4 dígitos");
            } else if (!userPasswords.containsKey(user)) {
                userPasswords.put(user, password);
                showAlert("Usuario " + user + " registrado con éxito");
            } else {
                showAlert("El usuario " + user + " ya existe");
            }
            clean();
        });
    }

    private void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Mensaje");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void clean() {
        tfUsuario.setText("");
        pfContraseña.setText("");
    }

    public static void main(String[] args) {
        launch();
    }
}
