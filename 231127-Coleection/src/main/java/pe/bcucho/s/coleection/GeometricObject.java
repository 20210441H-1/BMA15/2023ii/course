/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.coleection;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
import java.util.Date;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
public abstract class GeometricObject {
    private String color;
    private boolean filled;
    private java.util.Date creadtedDate;

    protected  GeometricObject(){
          this.color ="red";
          this.filled =false;
          creadtedDate= new java.util.Date();
          
    }

    protected GeometricObject(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
        creadtedDate= new java.util.Date();
    }
    
    
    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setCreadtedDate(Date creadtedDate) {
        this.creadtedDate = creadtedDate;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public Date getCreadtedDate() {
        return creadtedDate;
    }

    @Override
    public String toString() {
        return "GeometricObject{" + "color=" + color + ", filled=" + filled + ", creadtedDate=" + creadtedDate + '}';
    }
    
    public abstract double getArea();
    public abstract double getPerimeter();
}

