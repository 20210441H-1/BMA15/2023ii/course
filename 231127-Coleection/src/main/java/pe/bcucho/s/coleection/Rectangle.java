/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.coleection;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Rectangle extends GeometricObject {

    private double ancho;
    private double alto;

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public Rectangle() {
        this.alto=1;
        this.ancho=1;
    }

    public Rectangle(double ancho, double alto) {
        this.ancho = ancho;
        this.alto = alto;
    }

    public Rectangle(double ancho, double alto, String color, boolean filled) {
        super(color, filled);
        this.ancho = ancho;
        this.alto = alto;
    }
    
    
    @Override
    public double getArea() {
      return ancho*alto;
    }

    @Override
    public double getPerimeter() {
        return 2*(ancho+alto);
    }

    @Override
    public String toString() {
        return "Rectangule{" + "ancho=" + ancho + ", alto=" + alto + '}';
    }
    
}

