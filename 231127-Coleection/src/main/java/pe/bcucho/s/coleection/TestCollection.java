/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.coleection;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
public class TestCollection {

    public static void main(String[] args) {
        System.out.println("TestColeccion");
        String[] cities = {"Arequipa", "Lima", "Piera", "Oxapampa", "Callao", "Junin","Laxia","PEDU"};
        ArrayList<String> collecttion1 = new ArrayList<>();
        collecttion1.add(cities[0]);
        collecttion1.add(cities[1]);
        collecttion1.add(cities[2]);
        collecttion1.add(cities[3]);
      
            
        System.out.println("Una lista de ciuidades: " + collecttion1);
        System.out.println("La ciudad " + cities[3] + "esta presenta en la lista: "+ collecttion1.contains(cities[3]));
        
        if(collecttion1.contains(cities[4])){
            System.out.println("La ciudad esta contenida");
        }else{
            System.out.println("No esta contenida");
        }
        
        if(collecttion1.remove(cities[2])){
            System.out.println("Ha sido removido");
        }else{
            System.out.println("No ha sido removido");
        }
        
        System.out.println(collecttion1.size() + "ciudades presentes!");
        System.out.println(collecttion1);
        
        
        Collection<String> coleection2 = new ArrayList<>();
        coleection2.add(cities[5]);
        coleection2.add(cities[6]);
        coleection2.add(cities[7]);
        coleection2.add(cities[1]);
        
        System.out.println(coleection2);
        ArrayList<String> c1;
        c1 = (ArrayList<String>) collecttion1.clone(); //El clone no sabe 
        c1.addAll(coleection2);
        System.out.println(c1);
        System.out.println(coleection2);
        c1.removeAll(coleection2);
        System.out.println(c1);
        }
}
