/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.coleection;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public class TestSortStringByLength {
    public static void main(String[] args) {
        System.out.println("TestSortStringByLength!");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos"};
        
        for (String city : cities) {
            System.out.println(city + ", " + city.length());
        }
        
        System.out.println("ciudades: " + Arrays.toString(cities));
        Arrays.sort(cities, new MyComparator());
        System.out.println("ciudades: " + Arrays.toString(cities));
    }
    
    public static class MyComparator implements Comparator<String>{

        @Override
        public int compare(String o1, String o2) {
            return o1.length() - o2.length();
        }
    }
}

