/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tv;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TV {
    int channel1;
    int volumeLevel;
    boolean on;

    public boolean isOn() {
        return on;
    }
    
    //Limites
    final int minVolumeLevel=1;
    final int maxVolumeLevel=7;
    final int maxchannel=120;
    final int minchannel=1;
    
    //Super viene del papa 
    //Si es el extedns es la clase hija 
  
    public TV(){
        this.channel1=1;
        this.volumeLevel=3;
        this.on=false;
    }
    
    public void turnOn(){
         this.on = true;
    } 

    public void turnOff (){
         this.on = false;
    } 
    
    public void setChannel(int newChannel){
        this.channel1=newChannel;
    } 
    
    public void setVolume (int newVolumeLevel){
       this.volumeLevel=newVolumeLevel;
    } 
    //Se tiene que garantizar que se inicialice la clase papa con la hija  
    public void channelUp(){
         if(on && this.channel1<maxchannel){
             this.channel1 ++ ;
         } else if(this.channel1==maxchannel){
             this.channel1 = minchannel ;
         }
        
       
    } 

    public void channelDown(){
        if(on && this.channel1>minchannel){
             this.channel1 -- ;
         }else if(channel1==minchannel){
            this.channel1 = maxchannel;
        }
    }  
    
    public void volumeUp(){
        if(on && this.volumeLevel <maxVolumeLevel){
            this.volumeLevel ++ ;
        }
    } 

    public void volumeDown(){
        if(on && this.volumeLevel> minVolumeLevel){
             this.volumeLevel -- ;
        }
    } 

    @Override
    public String toString() {
        return "TV{" + "channel1=" + channel1 + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }
    
} 