/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tv;

import java.util.Scanner;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestTV {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        TV tv1 = new TV();
        tv1.turnOn();
       
        if(tv1.isOn()){
            int channel=120;
            int volume=7;
            System.out.println("Encendido!!!");
            tv1.setChannel(channel);
            tv1.setVolume(volume);
            System.out.println(tv1.toString());
            tv1.channelUp();
            System.out.println(tv1.toString());
            
        }else{
            System.out.println("Apagado!!!");
        }
     //   System.out.println(tv1.toString());
    
    
    }
    
    
}
