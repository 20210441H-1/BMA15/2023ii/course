/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package pe.bcucho.s.jalado;

/**
 *
 * @author PROFESOR
 */
public class Circle {
    double radius;
    
        Circle(){
            this.radius=1; //Sirve para referenciarse a si mismo
        }
        
        Circle(double  newradius){
            this.radius=newradius;
        }
        

    
        //Si hay 3 objetos y se intancia en difrentes numeros
        //El setter es void pero necesita parametros, a diferencia del getter que si tiene un valor de retorno pero no tiene funcion de parametro
        //El setter le indica le da parametros al objeto
        //Si era negativo debe ser privado, si es mas es publico  
    
    
    double getRadius(){
        return this.radius;
    }
    double getArea(){
        return Math.PI*Math.pow(this.radius, 2);
    }
    
    double getPerimetro(){
        return 2*Math.PI*this.radius;
    }
    
    void setRedius(double radius){
        //Ese parametro que entra lo vamos a copiar en el valor de nuestro campo
        this.radius=radius;
        //el proposito es ejecutarse en la instacion del objeto 
        //Los setter son para cambiar los valores
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + '}';
    }

    
}
