/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.clasesabstracts;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
public class test {
    public static void main(String[] args) {
        System.out.println("TestCirculeRectangule");
        Circle Circle1 = new Circle(5);
        Rectangule rectangulo = new Rectangule(5,5);
        System.out.println("El circulo" + Circle1.toString());
        System.out.println("El circulo" + rectangulo.getArea());
        System.out.println("El peri circulo" + rectangulo.getPerimeter());
        System.out.println("El area circulo" + rectangulo.toString());
        System.out.println("El area del rect es " + rectangulo.getArea());
        System.out.println("El perimetro del rect es " + rectangulo.getPerimeter());
    }
}
