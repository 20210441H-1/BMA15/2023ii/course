/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.clasesabstracts;

import java.util.Date;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
public abstract class GeometricObject {
    private String color;
    private boolean filled;
    private java.util.Date creadtedDate;

    protected  GeometricObject(){
          this.color ="red";
          this.filled =false;
          creadtedDate= new java.util.Date();
          
    }

    protected GeometricObject(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
        creadtedDate= new java.util.Date();
    }
    
    
    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setCreadtedDate(Date creadtedDate) {
        this.creadtedDate = creadtedDate;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public Date getCreadtedDate() {
        return creadtedDate;
    }

    @Override
    public String toString() {
        return "GeometricObject{" + "color=" + color + ", filled=" + filled + ", creadtedDate=" + creadtedDate + '}';
    }
    
    public abstract double getArea();
    public abstract double getPerimeter();
}


//La clase abstracta se diferencia , va a estar en cursiva. No se pueden inicializar
//Utilizaremos otro modificiador mishi -- protejido . Solo las pueden utilizar las hijas. Para los 
//Metidos es lo mismo 
//Cuando esta subrayado es cuando es estatico.


//Los metodos abstractos sirven para miplementar metodos (como el area de un circulo o rectangulo) pero que depende de cada objeto
//En otras palabas obliga a las hijas a q usen ese metodo (te dice que quieres pero no como)
//Herencia simple es solo de un clase  que se puede haeredar 
//La interfazr solo nevesita la firma o solo es esa es la firma 
//Interfaz es bueno ara hace integraciones 
//Una interfaz es como construir como una clae pero define operacion en comun para objetos, tambien se puede hacer un casteo
//Puede ser tipo de datos o una variable

//Las lineas punteadas y triangulos vacions son paa apuntar a una interfaz 