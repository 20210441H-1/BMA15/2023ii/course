/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.conditional;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Conditional {

    public static void main(String[] args) {
        System.out.println("Conditional statements!");

        int i = 35;
        if (30 < i) {
            System.out.println("el número es mayor a 30");
        }
        if (i % 2 == 1) {
            System.out.println("Es impar");
        }
        if (i % 2 == 0) {
            System.out.println("Es par");
        }

        System.out.println("------------------");

        int j = 31;
        if (30 < j) {
            System.out.println("el número es mayor a 30");
        } else {
            if (j % 2 == 1) {
                System.out.println("Es impar");
            } else {
                if (j % 2 == 0) {
                    System.out.println("Es par");
                }
            }
        }
        /*
        System.out.println("++++++++++++++");
        
        int k = 31;
        if (30 < k) {
            System.out.println("el número es mayor a 30");
            return;
        }
        if (k % 2 == 1) {
            System.out.println("Es impar");
            return;
        }
        if (k % 2 == 0) {
            System.out.println("Es par");
            return;
        }
        */
        
        int m = 12;
        switch (m%2) {
            case 0:
                System.out.println("Es par");
                break;
            case 1:
                System.out.println("Es impar");
                break;
            default:
                System.out.println("Cualquier otro número");
        }

    }

}
