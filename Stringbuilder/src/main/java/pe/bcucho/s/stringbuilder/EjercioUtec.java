/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.stringbuilder;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
import java.util.Scanner;
import javax.swing.JOptionPane;

public class EjercioUtec {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int number,n;
        System.out.println("Welcome to the Key-Generate");
        System.out.println("Please, enter the number of the Students ");
        n= entrada.nextInt();
        entrada.nextLine();
        StringBuilder[] apellidos=new StringBuilder[n];
        
        System.out.println("Ingrese el apellido:");
        for(int i=0;i<n;i++){
            System.out.print("Apellido[" + i + "] = ");
            String string=entrada.nextLine();
            apellidos[i] = new StringBuilder(); 
           apellidos[i].append(string);
        }
        System.out.println(" ");
        
        for(int i=0;i<n;i++){
            System.out.println("Alumno[" +i+ "]=" + apellidos[i]);
        }
        
       System.out.println("Please, enter the key lengtn");
        number=entrada.nextInt();
        StringBuilder[] codigos=new StringBuilder[n];
        entrada.nextLine();
        System.out.println("Ingrese los codigos de 8 digitos:");
        for(int i=0;i<n;i++){
            System.out.print("Apellido[" + i + "] = ");
            String string=entrada.nextLine();
            codigos[i] = new StringBuilder(number); 
           codigos[i].append(string);
        }
        System.out.println(" ");
        
        for(int i=0;i<n;i++){
            System.out.println("Alumno[" +i+ "]=" + codigos[i]);
        }
    }
}       
      

