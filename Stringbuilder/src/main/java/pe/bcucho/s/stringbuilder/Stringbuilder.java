/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.stringbuilder;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Stringbuilder {

    public static void main(String[] args) {
        //Diferencia entre string y strnigbuilder
        System.out.println("StringBuilder");
        String alpha = "";
        for (char character = 'a'; character <= 'z'; character++) {
            alpha += character;
        }
        System.out.println("alpha: " + alpha);
   
        
     // StringBuilder
        StringBuilder beta = new StringBuilder();
        for (char character = 'a'; character < 'z'; character++) {
            beta.append(character);
        }
        System.out.println("beta: " + beta);
        
        // constructs
        StringBuilder sb1 = new StringBuilder();
        
        StringBuilder sb2 = new StringBuilder("animal");
        StringBuilder sb3 = new StringBuilder(10);  // reservar un número de posiciones para caracteres
        
        //Append --> Agrega al final nuevas lineas 
        StringBuilder sb = new StringBuilder("star");
        sb.append("+ middle");
        StringBuilder same = sb.append("+end");
        System.out.println("same: " + same);
        System.out.println("sb: " + sb);
        
        StringBuilder a = new StringBuilder("abc");
        StringBuilder b = a.append("de");
        b = b.append("f").append("g");
        b.append("h");
        System.out.println("a: " + a);
        System.out.println("b: " + b);
        
        // substring() ---Devuelvela cadena comprendida entre esos 2 valores 
        StringBuilder sb4 = new StringBuilder("animals");
        String sub = sb4.substring(0, 5);
        System.out.println("sub: " + sub);
        
        //Indeoxof --Encuentre el indice con el valor deseado
        System.out.println(sb4.indexOf("a"));
        System.out.println(sb4.indexOf("al"));
        sub = sb4.substring(sb4.indexOf("a"), sb4.indexOf("al"));
        System.out.println("sub: " + sub);
        System.out.println("length: " + sb4.length()); //longitud
        System.out.println("length: " + sb4.capacity()); //capacidad
        char character = sb4.charAt(6); //Devuelve el caracter asociado
        System.out.println("character: " + character);
        
        
        // insert --Añade a partir del indice lo de la segunda cadena
        StringBuilder sb5 = new StringBuilder("animals");
        sb5.insert(7, "-");
        sb5.insert(0, "-");
        sb5.insert(4, "-");
        System.out.println("sb5: " + sb5);
        
        //Eliminar caracteres
        for(int i=0;i<sb5.length();i++){
            int aux='-';
            if(sb5.charAt(i)==aux){
                sb5.deleteCharAt(i);
            }
        }
        System.out.println("sb5: " + sb5);
        
        
        // delete
        StringBuilder sb6 = new StringBuilder("abcdef");
        sb6.deleteCharAt(5);
        System.out.println("sb6: " + sb6);
        sb6.delete(1, 3);
        System.out.println("sb6: " + sb6);
        
        
        // reverse
        StringBuilder sb7  =new StringBuilder("ABC");
        sb7.reverse();
        System.out.println("sb7: " + sb7);
        //Pasa de stringbuilder a string 
        String s = sb7.toString();
        System.out.println("s: " + s);
        
        //Cambia de caracter
        StringBuilder sb8 = new StringBuilder(10);
        sb8.append("Hola");
        sb8.setCharAt(2,'a');
        System.out.println("sb8= "+ sb8);
        
        //Modifica la longitud
        System.out.println(""+ sb8.capacity());
        System.out.println(""+ sb8.length());
        sb8.setLength(10);
        System.out.println(""+ sb8.length());
        System.out.println("sb8 "+ sb8);
        
        StringBuilder sb9 = new StringBuilder("Hola Soy Bryan");
        sb9.replace(0,4,"Como estan");
        System.out.println("sb9: "+sb9);
        
    }
}
