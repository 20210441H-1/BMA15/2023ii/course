/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.stringbuilder;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Practica {
    public static void main(String[] args) {
        StringBuilder string = new StringBuilder(29);
        string.append("Hola Caracola");
        System.out.println("El StringBuilder es: "+ string);
        System.out.println("Capacidad Inicial "+ string.capacity());
        System.out.println("Longitud Inicial "+ string.length());
        
        string.replace(0, 4, "Hay");
        string.append("s");
        System.out.println("El StringBuilder modificado es: "+ string);
        string.insert(4,"5000 ");
        System.out.println("El StringBuilder modificado es: "+ string);
        string.append(" en el mar");
        System.out.println("El StringBuilder modificado es: "+ string);
        String s= string.substring(string.length()-4,string.length());
        System.out.println("las ultimas 4 es:" + s);
        System.out.println("Capacidad final "+ string.capacity());
        System.out.println("Longitud final "+ string.length());
    }
}
