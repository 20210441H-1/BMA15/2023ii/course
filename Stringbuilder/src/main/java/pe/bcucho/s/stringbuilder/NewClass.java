/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.stringbuilder;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
import java.util.Scanner;

class NewClass {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        Estudiante alumno = IngresarDatos();
        ValidarClave(alumno);

    }

    static Estudiante IngresarDatos() {
        //Variables
        Estudiante Alumno;
        String Nombre, Apellido, Codigo, Clave;
        int n;
        //Ingreso de datos
        System.out.println("INGRESAR DATOS DEL ESTUDIANTE");
        System.out.println("Ingrese los nombres del estudiante");
        Nombre = sc.nextLine();
        System.out.println("Ingrese los apellidos del estudiante");
        Apellido = sc.nextLine();
        System.out.println("Ingrese el codigo del estudiante");
        Codigo = sc.nextLine();
        System.out.println("Ingrese la longitud de la clave");
        n = sc.nextInt();
        sc.nextLine(); // Consumir la nueva línea después de nextInt()
        do {
            System.out.println("Ingrese su clave de longitud " + n);
            Clave = sc.nextLine();
        } while (n != Clave.length());
        System.out.println();
        Alumno = new Estudiante(Nombre, Apellido, Codigo, Clave);
        //Retorno de Valores
        return Alumno;
    }

    static void ValidarClave(Estudiante Alumno) {
        String Clave;
        int n = 0;
        System.out.println("INGRESAR CLAVE DEL ESTUDIANTE");
        do {
            n++;
            if (n == 4) {
                System.out.println("BLOQUEO tras 3 intentos");
                break; // Salir del bucle después de 3 intentos
            }
            System.out.println("Ingrese la clave del estudiante");
            Clave = sc.nextLine();
        } while (!Clave.equals(Alumno.clave)); // Usar equals para comparar cadenas
        System.out.println("La clave en binario es: " + Alumno.ClaveBinario());
        System.out.println("La llave es: " + GenerarLlave(Alumno.ClaveBinario()));
    }

    static String GenerarLlave(String ClaveBinaria) {
        StringBuilder llaveBuilder = new StringBuilder();
        for (char bit : ClaveBinaria.toCharArray()) {
            if (bit == '1') {
                llaveBuilder.append("00");
            } else if (bit == '0') {
                llaveBuilder.append("11");
            }
        }
        return llaveBuilder.toString();
    }

}

class Estudiante {
    String nombres, apellidos, usuario;
    String codigo, clave;

    Estudiante(String nombres, String apellidos, String codigo, String clave) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.codigo = codigo;
        this.usuario = apellidos;
        this.clave = clave;
    }

    String ClaveBinario() {
        StringBuilder resultado = new StringBuilder();
        boolean par = true;
        for (char caracter : clave.toCharArray()) {
            if (Character.isLetter(caracter)) {
                if (Character.toLowerCase(caracter) == 'a' || Character.toLowerCase(caracter) == 'c') {
                    resultado.append('1');
                } else {
                    resultado.append('0');
                }
                par = !par;
            } else if (Character.isDigit(caracter)) {
                int digito = Character.getNumericValue(caracter);
                resultado.append(digito % 2 == 0 ? '0' : '1');
                par = !par;
            }
        }
        return resultado.toString();
    }

}