package pe.bcucho.s.ejercisio1;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.Collections;
import java.util.LinkedList;

public class App extends Application {

    private final LinkedList<String> nameList = new LinkedList<>();
    private final Label lbMessage = new Label("Ingrese un nombre: ");
    private final TextField tfName = new TextField();
    private final TextArea taNames = new TextArea();
    private final Button btSort = new Button("Ordenar");
    private final Button btShuffle = new Button("Aleatorio");

    @Override
    public void start(Stage stage) {
        HBox hBox = new HBox(10);
        hBox.getChildren().addAll(lbMessage, tfName);
        hBox.setAlignment(Pos.CENTER);

        HBox hBoxForButtons = new HBox(10);
        hBoxForButtons.getChildren().addAll(btSort, btShuffle);
        hBoxForButtons.setAlignment(Pos.CENTER);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(hBox);
        borderPane.setCenter(new ScrollPane(taNames));
        borderPane.setBottom(hBoxForButtons);

        var scene = new Scene(borderPane, 400, 220);
        stage.setScene(scene);
        stage.show();

        tfName.setOnAction(e -> {
            System.out.println("Ingresar nombre");
            String name = tfName.getText().trim(); // Obtener el nombre y eliminar espacios al inicio y al final
            if (!name.isEmpty() && !nameList.contains(name)) {
                nameList.add(name);
                displayNames();
            }
            tfName.setText("");
        });

        btSort.setOnAction(e -> {
            System.out.println("ordenar nombres");
            Collections.sort(nameList);
            displayNames();
        });

        btShuffle.setOnAction(e -> {
            System.out.println("nombres aleatorios");
            Collections.shuffle(nameList);
            displayNames();
        });
    }

    private void displayNames() {
        taNames.clear();
        for (String name : nameList) {
            taNames.appendText(name + "\n"); // Agregar cada nombre con un salto de línea
        }
    }

    public static void main(String[] args) {
        launch();
    }
}

//
//import javafx.application.Application;
//import javafx.scene.Group;
//import javafx.scene.Scene;
//import javafx.scene.paint.Color;
//import javafx.scene.shape.Circle;
//import javafx.scene.shape.Polygon;
//import javafx.stage.Stage;
//
//
//public class App extends Application {
//
//    @Override
//    public void start(Stage stage) {
//        // Crear un círculo con radio 50
//        Circle circle = new Circle(100, 100, 50);
//        circle.setFill(Color.BLUE); // Color del círculo
//        circle.setStroke(Color.BLACK); // Color del borde
//        circle.setStrokeWidth(2); // Ancho del borde
//
//        // Crear un triángulo con vértices definidos
//        Polygon triangle = new Polygon();
//        // Definir los vértices del triángulo
//        triangle.getPoints().addAll(150.0, 200.0,
//                                    50.0, 300.0,
//                                    250.0, 300.0);
//        triangle.setFill(Color.RED); // Color del triángulo
//        triangle.setStroke(Color.BLACK); // Color del borde
//        triangle.setStrokeWidth(2); // Ancho del borde
//
//        Group root = new Group(circle, triangle);
//        Scene scene = new Scene(root, 300, 400); // Crear una escena con las formas
//
//        stage.setTitle("Dibujando formas");
//        stage.setScene(scene);
//        stage.show();
//    }
//
//    public static void main(String[] args) {
//        launch();
//    }
//}