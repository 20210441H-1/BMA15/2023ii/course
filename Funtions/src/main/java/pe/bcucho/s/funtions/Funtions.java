/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.funtions;

import java.util.Arrays;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Funtions {

     public static void main(String[] args) {
        System.out.println("Functions!");
        Function();
        Function("Ronald");
        Function("Ronald", 45);
         
        
       
        String[] result = {"Hello ", "Ronald,", " you", " are", " 45", " years", " old", " !!!"};
        System.out.println(Function(result));
        System.out.println(Arrays.toString(result));
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i]);
        }
        System.out.println("");
        for (String abc: result) {
            System.out.print(abc);
        }
        System.out.println("");
        System.out.println("\nsuma: " + Function(1, 2, 3, 4, 5, 6));
    }
    
    public static void Function() {
        System.out.println("Inside the function !!!");
    }
    
    public static void Function(String name) {
        System.out.println("Inside the function " + name + " !!!");
    }
    
    public static void Function(String name, int age) {
        System.out.println("Hello " + name + ", you are " + String.valueOf(age) + " years old !!!");
    }
    
    public static String[] Function(String[] args) {
        return args;
    }
    
    public static int Function(int... numbers) {
        int total = 0;
        for (int number : numbers) {
            total += number;
            System.out.print(number + "\t");
        }
        return total;
    }

}
