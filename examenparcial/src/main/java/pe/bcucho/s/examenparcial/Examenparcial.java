/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.examenparcial;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
        //import java.util.scanner;
        // Scanner entrada = new Scanner(system in)
public class Examenparcial {
      java.util.Scanner input =new java.util.Scanner(System.in);
     
    public static void main(String[] args) {
        java.util.Scanner input =new java.util.Scanner(System.in);
        String[] string1; int[] edad;
        System.out.println("Ingrese el numero de apellidos");
        int n=input.nextInt();  
        System.out.println("");
        string1= new String[n];
        edad= new int[n];
        //instancia un objeto
        new Examenparcial().llenar(string1, n );
        System.out.println("El array es: ");
        new Examenparcial().imprimir(string1 , n);
        Arrays.sort(string1, Comparator.naturalOrder());
        System.out.println("Ell array ordenado: ");
        new Examenparcial().imprimir(string1, n);
        //System.out.println(Arrays.toString(string1));
        new Examenparcial().Ingresar(edad, n);
        new Examenparcial().ordenar(edad, n);
        new Examenparcial().imprimir2(edad, n);

    }
      
    private void llenar (String[] array1, int n){
        System.out.println("Ingrese los apellidos:");
        for(int i=0;i<n;i++){
            //Si quieres guardar todo la cadena con espacio es nextLine
            System.out.print("apellidos["+(i+1)+"]= ");
            array1[i]=input.next();
        }
    }   
    
    private void Ingresar (int[] array1, int n){
        System.out.println("Ingrese la edad:");
        for(int i=0;i<n;i++){
            //Si quieres guardar todo la cadena con espacio es nextLine
            System.out.print("Edad["+(i+1)+"]= ");
            array1[i]=input.nextInt();
        }
        
    }   
    
    private void imprimir(String[] array1, int n){
        for(int i=0;i<n;i++){
            if(i==0){
                System.out.print( "Apellidos[]= " + array1[i] );
            }
            else{
                if(i==(n)){
                    System.out.print(" "+ array1[i]);
                }
                else{
                    System.out.print(","+ array1[i]);
                }
            }
        }
        System.out.println(" ");
    }
    
      private void imprimir2(int[] array1, int n){
        for(int i=0;i<n;i++){
            if(i==0){
                System.out.print( "Edad[]= " + array1[i]);
            }
            else{
                if(i==n){
                    System.out.print(" "+ array1[i]);
                }
                else{
                    System.out.print(","+ array1[i]);
                }
            }
        }
        System.out.println(" ");
    }
      
    private void ordenar(int[] array1,int n){
        int aux;
        for(int i=0;i<n;i++){
            for(int j=i;j<n;j++){
                if(array1[i]>array1[j]){
                    aux=array1[i];
                    array1[i]=array1[j];
                    array1[j]=aux;
                }
            }
        }
    }
}
