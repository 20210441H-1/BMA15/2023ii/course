/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Pelicula {
    private String titulo;
    private String director;
    private int duracion;
    private Actor[] actores = new Actor[16];
    private int numactores;

    public Pelicula() {
        this.titulo="";
        this.director="";
        this.duracion=0;
        for(int i=0;i<16;i++){
            this.actores[i]= new Actor();
        }
        this.numactores=0;
    }
    
    public Pelicula(String newTitulo, String newDirector, int duracion) {
        this.titulo=newTitulo;
        this.director=newDirector;
        this.duracion=duracion;
        for(int i=0;i<16;i++){
            this.actores[i]= new Actor("",0);
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public String toString() {
        System.out.println("Los actores que trabajan en esta peli son ");
        for(int i=0;i<this.numactores;i++){
            System.out.print(this.actores[i].getNombre()+ ", ");
        }
        System.out.println("");
        return "Pelicula{" + "titulo=" + titulo + ", director=" + director + ", duracion=" + duracion + ", numactores=" + numactores + '}';
        
    }
    
   public boolean isActor(Actor actor){
       boolean find=false;
       for(int i=0;i<numactores;i++){
           if(actor.getNombre().equals(actores[i].getNombre()) && actor.getNacimiento()==actores[i].getNacimiento())
                       find=true;
           }
        return find;
    }
  
    public void introduceActor(Actor actor){
        if(this.numactores<16){
            this.actores[this.numactores++]=actor;
        }
    }
}