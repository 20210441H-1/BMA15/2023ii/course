/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Libro{
    private String titulo;
    private long isbn;
    private String autor;
    private int year;
    
    //Atribuos de la relacion de composicion
    private Paginas[] pagina;
    private int  numeropag;

    public Libro(String titulo, long isbn, String autor, int year) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.autor = autor;
        this.year = year;
        //Reservar espacio de memoria 
        this.pagina= new Paginas[999];
        //Reservamos espacio de memoria
        for(int i=0;i<999;i++){
            this.pagina[i]= new Paginas("",0);
        this.numeropag=0;
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    //Metodos para trabajar con la clase compuesta
    public int getNumeroPagi(){
        return this.numeropag;
    }
    
    public void addPage(Paginas newpage){
        if(this.numeropag<999){
            this.pagina[this.numeropag++]=newpage;
        }
    }
    
    public Paginas getPaginaNumero(int numpag){
        for(int i=0;i<this.numeropag;i++){
            if(this.pagina[i].getNumero()==numpag){
                return this.pagina[i];
            }
        }
        return null;
    }
}