/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestPelicula {
    public static void main(String[] args) {
        Pelicula la_guerra= new Pelicula("La guerra de las galaxias", "George Luchas", 121);
        Actor Mark = new Actor("Mark Hamill", 1951);
        Actor Harrison = new Actor("Harrison Ford", 1942);
        Actor Carrie = new Actor("Carrie Fisher", 1956);
        Actor alec = new Actor("Alec Guinness", 1914);
        
        la_guerra.introduceActor(Mark);
        la_guerra.introduceActor(Harrison);
        la_guerra.introduceActor(Carrie);
        la_guerra.introduceActor(alec);
        
        System.out.println(la_guerra.toString());
    }
}
