/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TesTAgregacion1 {
    public static void main(String[] args) {
        System.out.println("TestAgregacion1");
        Circunferencia circulo1 = new Circunferencia();
        System.out.println(circulo1.toString());
        circulo1.trasladarCircunferencia(3, 3);
        circulo1.setArea(3.1416);
        System.out.println(circulo1.toString());
    }
}
