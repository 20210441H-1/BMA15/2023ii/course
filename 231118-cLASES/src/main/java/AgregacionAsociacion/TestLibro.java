/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestLibro {
    public static void main(String[] args) {
        Libro el_quijote = new Libro("Don Quijote de la mancha", 1234567890, "Miguel de Cervantes" ,1605);
        Paginas pag1= new Paginas ("En algun lugar de la mancha, de cuyo...",1);
        Paginas pag2= new Paginas ("no ha mucho tiepom que vivie n hidalgo...",2);
        //Pasamos al objeto libro, los objetos del tipo pagina
        el_quijote.addPage(pag1);
        el_quijote.addPage(pag2);
     
        try{
            for(int i=1;i<=el_quijote.getNumeroPagi();i++){
            System.out.println(el_quijote.getPaginaNumero(i).getContenido());
            }
        }catch(NullPointerException e){
        System.out.println(e);
        }
        
    }
}