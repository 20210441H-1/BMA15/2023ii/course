/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

public class punto{
    private double coord_x;
    private double coord_y;

    //Constructores 

    public punto() {
        this.coord_x=0.0;
        this.coord_y=0.0;
    }
    
    public punto(double coord_x, double coord_y) {
        this.coord_x = coord_x;
        this.coord_y = coord_y;
    }
    
    
    //Metodos
    public double getCoord_x() {
        return this.coord_x;
    }

    public void setCoord_x(double coord_x) {
        this.coord_x = coord_x;
    }

    public double getCoord_y() {
        return this.coord_y;
    }

    public void setCoord_y(double coord_y) {
        this.coord_y = coord_y;
    }
    
    
    
}