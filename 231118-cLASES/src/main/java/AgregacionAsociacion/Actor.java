/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Actor {
    private String nombre;
    private int nacimiento;

    public Actor(String nombre, int nacimiento) {
        this.nombre = nombre;
        this.nacimiento = nacimiento;
    }

    public Actor() {
        this.nombre="";
        this.nacimiento=0;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(int nacimiento) {
        this.nacimiento = nacimiento;
    }

    @Override
    public String toString() {
        return "Actor{" + "nombre=" + nombre + ", nacimiento=" + nacimiento + '}';
    }
    
    
}
