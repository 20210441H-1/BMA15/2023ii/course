/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package AgregacionAsociacion;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */

public class Circunferencia {
    private double radio;
    private final static double PI=3.1416;
    private punto centro;
    
    //Constructores

    public Circunferencia() {
        this.radio=0;
        this.centro= new punto(); //Se llama al constructor para evitar
        //Un nullJavaPointer
    }
    
    
    public Circunferencia(double radio1, double cx, double cy) {
        this.radio = radio1;
      this.centro = new punto(cx, cy);
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double getDiameter(){
        return 2*this.radio;
    }

    public void setDiameter(double diametro){
        radio=diametro/2;
    }
    
    public double getLongitud(){
        return 2*PI*this.radio;
    }
    
    public void setLongitud(double longitud){
        this.radio= longitud/(2*PI);
    }
    
    public double getArea(){
        return Math.pow(this.radio, 2)*PI;
    }
    
    public void setArea(double area){
        this.radio=  Math.sqrt(area/PI);
    }
    
    public double getxCentro(){
        return this.centro.getCoord_x();
    }
    
    public void setxCentro(double xcentro){
       this.centro.setCoord_x(xcentro);
    } 
    
    public void setyCentro(double ycentro){
       this.centro.setCoord_y(ycentro);
    }
    
    public double getyCentro(){
        return this.centro.getCoord_y();
    }
    
    public void trasladarCircunferencia(double newx, double newy){
        this.centro.setCoord_x(this.centro.getCoord_x()+ newx);
        this.centro.setCoord_y(this.centro.getCoord_y()+ newy);
    }

    @Override
    public String toString() {
        return "Circunferencia{" + "radio=" + radio + ", centro=(" + centro.getCoord_x() + ',' + centro.getCoord_y()+ ")}";
    }
    
}