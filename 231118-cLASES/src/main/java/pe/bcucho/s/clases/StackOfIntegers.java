/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.clases;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class StackOfIntegers {

    //fields
    private int[] elements;
    private int size;
    private static final int defaultCapacity=16;
    //Constructor
    public StackOfIntegers(){
//        elements = new int[defaultCapacity];
           this(defaultCapacity);
    }
    
    public StackOfIntegers(int capacity){
        elements = new int[capacity];
    }
    
    public boolean empty(){
        return (size == 0);
    }
    
    public int peek(){
        return elements[size-1];
    }
    
    public int getSize(){
        return size; 
    }
    
    public void push(int value ){
        if(elements.length>=size){
            elements[size++] = value;
            //     size++;
        }
    }
    
    public int pop(){
       int topvalue = elements[size-1];
       elements[size-1]=0;
       size--;
    return topvalue; //Para implementar
    }
}
