/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.clases;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Circle extends GeometricObject {
    private double radious;
    
    public Circle(){
        
    }
    
    public Circle (double radious){
        this.radious=radious;
    }
    
    public Circle(double radious, String color, boolean filled){
        super(color, filled);
        this.radious=radious;
    }
    
    public double getRadious() {
        return radious;
    }

    public void setRadious(double radious) {
        this.radious = radious;
    }

    double getDiameter() {
        return 2 * this.radious;
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }

    double getArea() {
        return Math.PI * this.radious * this.radious;
    }
    
    void printCircle(){
        System.out.println("Creación: " + getDateCreated() + ", radio: " + this.radious);
    }
    
    
}
