/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.clases;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestCircleRectangule {
    public static void main(String[] args) {
        Circle circle = new Circle(1);
        System.out.println("El círculo: " + circle.toString());
        System.out.println("El color: " + circle.getColor());
        System.out.println("El radio: " + circle.getRadious());
        System.out.println("El área: " + circle.getArea());
        System.out.println("El diámetro: " + circle.getDiameter());
    }
}

