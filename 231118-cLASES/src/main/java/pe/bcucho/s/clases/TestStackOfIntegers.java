/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.clases;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class TestStackOfIntegers {
    public static void main(String[] args) {
        StackOfIntegers stack = new StackOfIntegers();
        for( int i=0;i<16;i++){
            stack.push(i);
        }
       
        System.out.println(stack.pop());
    }
}
