/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class EmpleadoDistribucion extends Asalariado {
    private String zona;

    public EmpleadoDistribucion(String zona, String nombre, long dni, int diasvacaciones , double sala) {
        super(nombre, dni, diasvacaciones, sala);
        this.zona = zona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    } 
    
    public double getSalario(){
        return super.getSalario()*(1+0.15);
    }
}
