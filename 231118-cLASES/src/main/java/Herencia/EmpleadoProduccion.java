/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class EmpleadoProduccion extends Asalariado {
    private String turno;
    private int numero;
    public EmpleadoProduccion(String turno, String nombre, long dni, int diasvacaciones, double salara) {
        super(nombre, dni, diasvacaciones, salara);
        this.turno = turno;
        this.numero=0;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }  
    
    public void addAsalariado(Asalariado a, Asalariado[] array){
        array[this.numero++]=a;
    }
    
    public double getSalario(){
        return super.getSalario()*(1+0.15);
    }
}
