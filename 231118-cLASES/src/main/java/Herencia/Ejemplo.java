/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Ejemplo {
    public static void main(String[] args){
        Asalariado emplead1 = new Asalariado("Manuel", 1233, 28,121);
        EmpleadoDistribucion emplead3 = new EmpleadoDistribucion("NOCHE", "Juanito", 321, 0,121);
        EmpleadoProduccion emplead2 = new EmpleadoProduccion("Granada", "Pepe", 322121, 0,121);
    
//        System.out.println("Name:" + emplead1.getNombre());
//        System.out.println("Name:" + emplead2.getNombre());
//        System.out.println("Region:" + emplead2.getZona());
//        System.out.println("Name:" + emplead3.getNombre());
//        System.out.println("Region:" + emplead3.getZona());
        
        Asalariado[] array_asa = new Asalariado[3];
        for(int i=0;i<3;i++){
            array_asa[i]= new Asalariado("",0,0,0);
        }
        
        emplead2.addAsalariado(emplead1, array_asa);
        for(int i=0;i<3;i++){
            System.out.println(array_asa[i].getNombre());
        }
    }
}
