/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class Asalariado {
    private String nombre;
    private long dni;
    private int diasvacaciones;
    private double salarioBase;

    protected Asalariado(String nombre, long dni, int diasvacaciones, double newslaria) {
        this.nombre = nombre;
        this.dni = dni;
        this.diasvacaciones = diasvacaciones;
        this.salarioBase=newslaria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public int getDiasvacaciones() {
        return diasvacaciones;
    }

    public void setDiasvacaciones(int diasvacaciones) {
        this.diasvacaciones = diasvacaciones;
    }
    
    public double getSalario(){
        return this.salarioBase;
    }
}
