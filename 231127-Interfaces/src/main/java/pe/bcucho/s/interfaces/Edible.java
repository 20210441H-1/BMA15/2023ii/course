/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.bcucho.s.interfaces;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
public interface Edible {
    //En una interface siemore es abstracta, solo se generan firmas
    //No sabemos como se va a implementar el objeto 
    public  String howtoEat();
}
