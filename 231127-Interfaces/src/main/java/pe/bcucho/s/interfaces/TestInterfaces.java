/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.interfaces;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */
//Clase padre de objetct  , puede manejar los distintos objetos. 

public class TestInterfaces {

    public static void main(String[] args) {
        System.out.println("TestInterfaces");
    Object[] objet = {new Orange(), new Apple(), new Chicken(), new Tiger()}; //Simplemente es para contener no que el 0 es del tipo orange
        for(Object element: objet){
           if(element instanceof Animal){
               System.out.println(((Animal) element).sound());
               
           }
           if(element instanceof Edible){
               System.out.println(((Edible) element).howtoEat());
           }
        }
    }
   //interfaz 1 hereda de esas 2 interfaces pero las clases las implementa 2 
    //Dato guarda la referencia , la instancia es el objeto en sí
    //cOLECCION --- almacena objetos 
    //La interfaz es la variable , podemos hablar de colas y asi 
    
}
