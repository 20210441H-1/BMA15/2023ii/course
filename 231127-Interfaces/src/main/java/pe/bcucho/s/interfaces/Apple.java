/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.interfaces;

/**
 *
 * @author Bryan CuchO Suyo <bryan.cucho.s@uni.pe>
 */


public class Apple extends Fruit {

    @Override
    public String howtoEat() {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          return "Apple: make a Struddle!";
    }
    
}
