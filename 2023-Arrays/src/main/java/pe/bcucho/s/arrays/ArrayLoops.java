/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.bcucho.s.arrays;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class ArrayLoops {
    public static void main(String[] args) {
        System.out.println("Array loops !!!");
        for (int i = 0; i < args.length; i++) {
            System.out.println(Integer.parseInt(args[i]));
        }
        
        System.out.println("Ingrese los valores a procesar que sean diferentes de cero: ");

        java.util.Scanner input = new java.util.Scanner(System.in);
        int[] numeros = new int[1000];
        int item = 1;
        int counter = 0;
        do {
            item = input.nextInt();
            if (item != 0) {
                numeros[counter] = item;
                counter++;
                System.out.println("item: " + item);
            }
        } while (item != 0);
        System.out.println("counter: " + counter);
        for (int i = 0; i < counter; i++) {
            System.out.print(numeros[i] + " ");
        }
    }

}
