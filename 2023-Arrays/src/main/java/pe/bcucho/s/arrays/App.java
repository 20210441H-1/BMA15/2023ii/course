/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.arrays;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Arrays");
        int arraySize=5;  //El array no es un tipo de datos o concatenacion
        double[] array3;
        double stylecppArray[];
        
            //Inicializar
        stylecppArray = new double[arraySize];
        array3 = new double[arraySize];
        
        // { , , , , } Por defecto se llevan con ceros
        //Defincion establecido
        array3[0]=5;
        array3[1]=4;
        array3[2]=3;
        array3[3]=2;
        array3[4]=1;
        
        for(int i=0;i<array3.length;i++){
        System.out.println("array[" + i +"]= " + array3[i]);
        }
        
         //Definir con un for
         System.out.println("Definicion");
        for(int i=0;i <stylecppArray.length;i++){
            stylecppArray[i]=i;
           }
        System.out.println("Visualizando");
        for(int i=0;i<stylecppArray.length;i++){
        System.out.println("array[" + i +"]= " + stylecppArray[i]);
        }
        
        //Inicializando de otra manera
        double[] myArray={1.6,2.5,3.5,4.7};
        for(int i=0;i<myArray.length;i++){
        System.out.println("array[" + i +"]= " + myArray[i]);
        }
        
        //Ingresar sus elementos, paraello existe en la clase scanner
        //O se importa la librera o solo esa funcion    
        java.util.Scanner input= new java.util.Scanner(System.in);
        //Eso se guarda en un array;
        System.out.println("");
        System.out.println("Ingrese "+myArray.length + "valores");
        for(int i=0;i<myArray.length;i++){
            myArray[i]= input.nextDouble();
        }        
        System.out.println("Visualizando");
        for(int i=0;i<myArray.length;i++){
        System.out.println("array[" + i +"]= " + myArray[i]);
        }
        
        //Inicializando valoress random con datos por usuario
        int hola[];
        int a;
        System.out.println("Ingrese el tamaño del array");
        a=input.nextInt();
        hola = new int[a];
        for(int i=0;i<hola.length;i++){
            hola[i]= (int) (1 + Math.random()*5);
        }
        for(int i=0;i<hola.length;i++){
            System.out.println("Hola: " + hola[i]);
        }
        //Suma
        int suma=0;
        for(int i=0;i<hola.length;i++){
            suma +=hola[i];
        }
        //Promedio
        double total=0; double promedio=0;
        for(int i=0;i<hola.length;i++){
            total += hola[i];
        }
        promedio = total/hola.length;
        System.out.println("promedio" + promedio);
        
        //Encontrar el mayor numero
        int max=hola[0];
        for(int i=0;i<hola.length;i++){
            if(max<hola[i]){
                max=hola[i];
            }
        }
        System.out.println("El maximo es :" + max);
        
        // mezclar elementos
        for (int i = 0; i < myArray.length; i++) {
            // generar un index aleatorio
            int j = (int) (Math.random() * myArray.length);
//            System.out.println("j:" + j);
            double temp = myArray[i];
            myArray[i] = myArray[j];
            myArray[j] = temp;
        }
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }

        // Shifting a la izquierda
        // [10, 20, 30, 40, 50]
        // [20, 30, 40, 50, 10]
        double temp = myArray[0];   // retiene el primer elemento
        for (int i = 1; i < myArray.length; i++) {
            myArray[i - 1] = myArray[i];
        }
        myArray[myArray.length - 1] = temp;
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }

        //
        String[] months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

        System.out.println("Ingresa un número del mes (1 al 12): ");
        int monthNumber = input.nextInt();
        System.out.println("El mes es " + months[monthNumber - 1]);
    }
}


