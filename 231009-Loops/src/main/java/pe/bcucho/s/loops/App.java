/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.bcucho.s.loops;

/**
 *
 * @author Bryan Cucho <bryan.cucho.s@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Loops 1");
        for (int i = 0; i < 10; i++) {
            System.out.print(i + ", ");
        }
        System.out.println("");

        System.out.println("Loop 2");
        for (int i = 10; 0 < i; i--) {
            System.out.println("i: " + i);
        }

        /*
        System.out.println("Loop 3");
        for (int i = 0; ; i++) {
            System.out.println("i: " + i);
        }
         */
        // A[5, 3]
        // A(1, 1), A(1, 2), A(1,3)
        // A(2, 1), A(2, 2), A(2,3)
        // A(3, 1), A(3, 2), A(3,3)
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 3; j++) {
                System.out.print("(" + i + "," + j + ")\t");
            }
            System.out.println("");
        }

        System.out.println("while loop 1");
        int i = 1;
        while (i <= 5) {
            System.out.println("i:" + i);
            i++;
        }

        System.out.println("while loop 2");
        int j = 5;
        while (1 <= j) {
            System.out.println("j:" + j);
            j--;
        }

        /*
        System.out.println("while loop 3");
        int k = 1;
        while (true) {
            System.out.println("k:" + k);
            k++;
        }
         */
        int a = 1;
        while (a <= 5) {
            int b = 1;
            while (b <= 3) {
                System.out.print("(" + a + "," + b + ")\t");
                b++;
            }
            System.out.println("");
            a++;
        }

        System.out.println("do while loop");
        int p = 1;
        do {
            int q = 1;
            do {
                System.out.print("(" + p + "," + q + ")\t");
                q++;
            } while (q <= 3);
            System.out.println("");
            p++;
        } while (p <= 5);
    }

}
